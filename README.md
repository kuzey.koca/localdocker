# Software Development with Docker

## Prerequisites
Ensure Docker engine and compose plugin are installed. Docker Desktop application will install these automatically for Windows and MacOS. For Linux/Ubuntu, follow the relevant tutorial or install Docker Desktop application.

Please follow the full document tutorial on confluence page. https://kkoca.atlassian.net/wiki/spaces/SD
